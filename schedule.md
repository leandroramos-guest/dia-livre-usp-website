---
layout: page
title: Programação
permalink: /schedule/
---

Temos uma proposta que mistura palestras e ofinas para o Dia Livre, confira:

## Palestras

* Abertura: O que é o Debian e Software Livre (Rodrigo Siqueira)
* Software livre na era dos fakenews (Kretcheu)
* Dos primórdios do Software livre brasileiro até questões do uso de recursos públicos (Paulo Meirelles)
* Hackeando os gastos públicos da USP. Em busca de uma saída mais eficiente com software livre. (Ricardo Fontoura e Thiago Veríssimo)

## Oficinas

* Install Fest - Instalação do Debian e de softwares livres (Anderson, Kretcheu, Leandro, Ricardo)

![Cartaz do Dia Livre 2018](/images/dia-livre-flyer.jpg "Cartaz do Dia Livre 2018")
